export const pathsitalo = {
  Italo: "/italo",
  ItaloInfo: "/italo/contact",
  ItaloAbout: "/italo/about",
  ItaloExperience: "/italo/experience",
};

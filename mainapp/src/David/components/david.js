import React, {Component} from 'react';
import {  Link } from "react-router-dom";
import { Menu,Container, Divider, Flag, Card, Image, Grid} from 'semantic-ui-react'
import { paths} from "../../Commons/dictionaries/urls"

class David extends Component {

    componentDidMount () {
    }
    render () {
        return (
            <div className="todo">
                <Menu >
                    <Menu.Item style={{marginLeft:"90px", color:'black'}}><Link to={paths.David}>David</Link></Menu.Item>
                    <Menu.Item><Link to={paths.Estadisticas}>Estadisticas</Link></Menu.Item>
                </Menu>
                <Container textAlign='left'>
                <Flag name='pe' /> Perfil 
                </Container>
                
                <Divider></Divider>
                <Grid divided='vertically'>
                    <Grid.Row columns={2}>
                        <Grid.Column width={6}>
                            <Card style={{marginLeft:"90px"}} >
                                <Image src='https://i.ibb.co/9Nzszvn/Foto-20194529-G.jpg' wrapped ui={false} />
                                <Card.Content>
                                <Card.Header><a href="/">David Yanccehuallpa</a></Card.Header>
                                <Card.Meta>Universidad Nacional de Ingenieria</Card.Meta>
                                <Card.Description>
                                    Soy un apasionado al codigo, optimizacion y a los retos que conllevan crear una aplicacion.
                                </Card.Description>
                                </Card.Content>
                                <Card.Content extra>
                                </Card.Content>
                            </Card>
                        </Grid.Column>
                        <Grid.Column>
                            <Container style={{paddingRight:"140px"}}>
                                <br></br>
                                Una breve descripcion ... Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of 
                                type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into
                                 electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets
                                  containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker
                                   including versions of Lorem Ipsum.
                            </Container>
                        </Grid.Column>
                    </Grid.Row>
                    
                </Grid>
                

            </div>
        )
        
    }
    
}

export default David;
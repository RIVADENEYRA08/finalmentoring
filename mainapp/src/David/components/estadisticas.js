import React, {Component} from 'react';
import {  Link } from "react-router-dom";
import { Menu, Button, Input, Dropdown, Grid, Label } from 'semantic-ui-react'
import { paths} from "../../Commons/dictionaries/urls"
import Chart from "react-apexcharts";
const dias = [
  { key: 1, text: '1991', value: 1 },
  { key: 2, text: '1992', value: 2 },
  { key: 3, text: '1993', value: 3 },
  { key: 4, text: '1994', value: 4 },
  { key: 5, text: '1995', value: 5 },
  { key: 6, text: '1996', value: 6 },
  { key: 7, text: '1997', value: 7 },
  { key: 8, text: '1998', value: 8 }
]


class Estadisticas extends Component {
    constructor(props) {
        super(props);
        //this.addFirst = this.addFirst.bind(this)
        //this.handleInputChange = this.handleInputChange.bind(this)
        this.state = {
          indiceAnio:0,
          incrementador:6,
          options: {
            chart: {
              id: "basic-bar"
            },
            xaxis: {
              categories: [1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998]
            }
          },
          series: [
            {
              name: "series-1",
              data: [32, 40, 45, 50, 49, 60, 70, 91]
            }
          ]
        };
        
    }
    handleInputChange = (value = 1) =>{
      this.setState({
        incrementador: value
      });
    }
    
    addFirst = ()=>{
      const incrementador = parseInt(this.state.incrementador) || 0;
      let array=[];
      this.state.series[0].data.forEach((cantidad,indice) => {
        if(indice === this.state.indiceAnio){
          array.push(cantidad+incrementador)
        }
        else{
          array.push(cantidad)
        } 
      })
      const serie = [{name:"Serie", data:array}] 
      this.setState({
        series:serie
      })
    }
    addYear = (e)=>{
      const anio= parseInt(e.target.getElementsByClassName("text")[0].innerHTML)
      let indiceAnio = anio-1991;
      this.setState({indiceAnio: indiceAnio})
    }

    componentDidMount () {
    }
    render () {
        const {valor1}= this.state;
        return (
            <div className="todo" >
                
                <Menu>
                    <Menu.Item style={{marginLeft:"90px"}}><Link to={paths.David}>David</Link></Menu.Item>
                    <Menu.Item><Link to={paths.Estadisticas}>Estadisticas</Link></Menu.Item>
                </Menu>
                  <div style={{marginLeft:"90px"}}>
                    <Grid columns={2}>
                      <Grid.Row>
                        <Grid.Column width={3}>
                          <Input placeholder="Ingrese incrementador" value={this.state.incrementador} onChange={(event)=>this.handleInputChange(event.target.value)}></Input>
                          <Label pointing >Incrementador de unidades </Label>
                        </Grid.Column>
                        
                      </Grid.Row> 
                      <Grid.Row>
                        <Grid.Column width={3}>
                          <Dropdown placeholder='Seleccione año' options={dias} search selection onChange={(e)=>this.addYear(e)} value={valor1}/>
                        </Grid.Column>
                        <Grid.Column textAlign='left' >
                          <Button onClick={()=>this.addFirst()}>Incrementar</Button>
                        </Grid.Column> 
                      </Grid.Row> 
                    </Grid>
                    <br></br>
                    <Chart
                        options={this.state.options}
                        series={this.state.series}
                        type="bar"
                        width="500"
                    />
                  </div>
                
            </div>
            

        )
        
    }
    
}

export default Estadisticas;
import "./App.css";
import React from "react";
import { Menu } from "semantic-ui-react";

import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { pathsitalo } from "./Commons/dictionaries/constantesitalo";
import { paths } from "./Commons/dictionaries/urls";
import "semantic-ui-css/semantic.min.css";
//Semantic UI
// importaciones

//Seccion de David
import David from "./David/components/david";
import Estadisticas from "./David/components/estadisticas";
import { Italo } from "./Italo/components/italo";
//Seccion de Raul
import { pathsRaul } from "./Commons/dictionaries/constantesRaul";
import { HomeRaul } from "./Raul/component/HomeRaul";
import Servicios from "./Raul/component/Service";
import Almacen from "./Raul/component/ServiciosRaul/Almacen";
import Publicidad from "./Raul/component/ServiciosRaul/Publicidad";
import Seleccion_Personal from "./Raul/component/ServiciosRaul/Seleccion de Personal";


import HomeAngel from "./Angel/HomeAngel"
import { pathsAngel } from "./Commons/dictionaries/constantesAngel";


function App() {
  return (
    <Router>
      <Menu style={{ marginBottom: "0px" }}>
        <Menu.Item name="path-david">
          <Link to={paths.David}>David</Link>
        </Menu.Item>
        <Menu.Item name="editorials">
          <Link to={paths.Italo}>Italo</Link>
        </Menu.Item>
        <Menu.Item name="editorials">
          <Link to="/Gonzalo">Gonzalo</Link>
        </Menu.Item>
        <Menu.Item name="editorials">
          <Link to={pathsAngel.HomeAngel}>Angel</Link>
        </Menu.Item>
        <Menu.Item name="editorials">
          <Link to={pathsRaul.Raul}>Raul</Link>
        </Menu.Item>
      </Menu>
      <Switch>
        <Route
          component={() => (
            <Switch>
              <Route exact path={paths.David} component={David} />
              <Route exact path={paths.Estadisticas} component={Estadisticas} />
              <Route exact path={pathsitalo.Italo} component={Italo} />
              <Route exact path="/Gonzalo" />
              <Route exact path={pathsAngel.HomeAngel} component={HomeAngel} />
              <Route exact path={pathsRaul.Raul} component={HomeRaul} />
              <Route exact path={pathsRaul.Services} component={Servicios} />
              <Route exact path={pathsRaul.Almacen} component={Almacen} />
              <Route exact path={pathsRaul.Publicidad} component={Publicidad} />
              <Route exact path={pathsRaul.SeleccionPersonal} component={Seleccion_Personal} />
            </Switch>
          )}
        />
      </Switch>
    </Router>
  );
}

export default App;

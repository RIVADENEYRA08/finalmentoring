import React from "react";
import { Link } from "react-router-dom";
export function Header() {
  return (
    <>
      <div class="ui menu">
        <Link to="/italo">
          <div class="header item">ItaloReact</div>
        </Link>
        <Link to="/italo/about">
          <a class="item">About</a>
        </Link>
        <Link to="/italo/experience">
          <a class="item">Experience</a>
        </Link>
        <Link to="/italo/contact">
          <a class="item">Contact</a>
        </Link>
      </div>
    </>
  );
}

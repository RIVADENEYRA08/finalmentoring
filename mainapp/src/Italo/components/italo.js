import React, { Component } from "react";
import { Route, Router, Switch } from "react-router";
import { Header } from "./Header";
import { Welcome } from "./Pages/Welcome";
import { About } from "./Pages/About";
import { Experience } from "./Pages/Experience";
import { Info } from "./Pages/Info";
import { pathsitalo } from "../../Commons/dictionaries/constantesitalo";
// import { Menu } from "semantic-ui-react";
// import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
export const Italo = () => {
  return (
    <>
      <div>
        <Switch>
          <Header />

          <Switch>
            <Route exact path={pathsitalo.Italo} component={Welcome}>
              <About></About>
            </Route>
            <Route exact path={pathsitalo.ItaloAbout} component={About} />
            <Route
              exact
              path={pathsitalo.ItaloExperience}
              component={Experience}
            />
            <Route exact path={pathsitalo.ItaloInfo} component={Info} />
          </Switch>
        </Switch>
      </div>
    </>
  );
};

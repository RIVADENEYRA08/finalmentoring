import React, { Component } from 'react';
import './style.css';

import { Table} from 'semantic-ui-react';
import {Link} from "react-router-dom";
import { Menu} from 'semantic-ui-react'
import { pathsRaul } from '../../Commons/dictionaries/constantesRaul';


class Servicios extends Component{
    componentDidMount() {
    }
    render() {
        return (
        <div>
            <Menu >
                <Menu.Item style={{marginLeft:"90px", color:'black'}}><Link to={pathsRaul.Raul}>Raul</Link></Menu.Item>
                <Menu.Item>
                    <Link to={pathsRaul.Services}>Servicios</Link>
                </Menu.Item>
            </Menu>   
            <div className="block">
                <h1 className="Title">Nuestros servicios </h1>
                <div className="Table">
                    <Table celled>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell>Nombre</Table.HeaderCell>
                                <Table.HeaderCell>Costo</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>
                        <Table.Body>
                            <Table.Row>
                                <Table.Cell>
                                    <Link to={pathsRaul.Publicidad}>
                                        <button className="ui primary button">Publicidad</button>
                                    </Link>
                                </Table.Cell>
                                <Table.Cell>s/50</Table.Cell>
                            </Table.Row>
                            <Table.Row>
                                <Table.Cell>
                                    <Link to={pathsRaul.Almacen}>
                                        <button className="ui primary button">Almacen</button>
                                    </Link>
                                </Table.Cell>
                                <Table.Cell>s/20</Table.Cell>
                            </Table.Row>
                            <Table.Row>
                                <Table.Cell>
                                    <Link to={pathsRaul.SeleccionPersonal}>
                                        <button className="ui primary button">Seleccion de Personal</button>
                                    </Link>
                                </Table.Cell>
                                <Table.Cell>s/25</Table.Cell>
                            </Table.Row>
                        </Table.Body>               
                    </Table>
                </div>
            </div>    
        </div>         
        );
      }
    }
export default Servicios;


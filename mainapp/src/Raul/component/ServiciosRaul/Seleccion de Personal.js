import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { pathsRaul } from '../../../Commons/dictionaries/constantesRaul';
import Personal from "../../assets/SeleccionPersonal.gif"
import "../../component/style.css"
 class Seleccion_Personal extends Component{
    componentDidMount() {
    }
    render() {
        return (
           <div className="blockservices">
                <div class="ui link cards">
                    <div className="card">
                        <div className="image">
                            <img src={Personal} alt="almacen"/>
                        </div>
                        <div className="content">
                            <div className="header">Seleccion de Personal</div>
                            <div className="description">Servicio dedicado conseguir un personal bajo ciertas limitaciones y que cumpla cierto requerimientos</div>
                        </div>
                        <div className="extra content">
                            <span className="right floated">Duracion 2 semanas </span>
                            <span><i className="user icon"></i> 325 ticket</span>
                        </div>
                    </div>
                </div>
                <Link to={pathsRaul.Services}>
                    <button className="ui primary button">Regreso</button>
                </Link>
            </div>     
        )
    }
 }
 export default Seleccion_Personal;
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { pathsRaul } from '../../../Commons/dictionaries/constantesRaul';
import publicidad from "../../assets/Publicidad.gif"
import "../../component/style.css"
 class Publicidad extends Component{
    componentDidMount() {
    }
    render() {
        return (
           <div className="blockservices">
                <div class="ui link cards">
                    <div className="card">
                        <div className="image">
                            <img src={publicidad} alt="almacen"/>
                        </div>
                        <div className="content">
                            <div className="header">Publicidad</div>
                            <div className="description">Servicio dedicado a maximizar la audencia minimizando los costos a travez de un plan publicitario</div>
                        </div>
                        <div className="extra content">
                            <span className="right floated">Duracion determinado por el cliente </span>
                            <span><i className="user icon"></i> 100 canales </span>
                        </div>
                    </div>
                </div>
                <Link to={pathsRaul.Services}>
                    <button className="ui primary button">Regreso</button>
                </Link>
            </div>     
        )
    }
 }
 export default Publicidad;
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { pathsRaul } from '../../../Commons/dictionaries/constantesRaul';
import almacen from "../../assets/Almacen.gif"
import "../../component/style.css"
 class Almacen extends Component{
    componentDidMount() {
    }
    render() {
        return (
           <div className="blockservices">
                <div class="ui link cards">
                    <div className="card">
                        <div className="image">
                            <img src={almacen} alt="almacen"/>
                        </div>
                        <div className="content">
                            <div className="header">Almacen</div>
                            <div className="description">Servicio dedicado al guardado de un producto maximizando la conservación del producto </div>
                        </div>
                        <div className="extra content">
                            <span className="right floated">Duracion determinado por el cliente</span>
                            <span><i className="user icon"></i> 320 ticket</span>
                        </div>
                    </div>
                </div>
                <Link to={pathsRaul.Services}>
                    <button className="ui primary button">Regreso</button>
                </Link>
            </div>     
        )
    }
 }
 export default Almacen;
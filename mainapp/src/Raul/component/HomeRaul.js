import photo from "../assets/foto.JPG"
import { Menu} from 'semantic-ui-react'
import './style.css';
import { pathsRaul } from "../../Commons/dictionaries/constantesRaul";
import { Link } from "react-router-dom";
export const HomeRaul = ()=>{
    function gitlab() {
        window.open("https://gitlab.com/raul.rivera.c");
      }
      function linkedin() {
        window.open("https://www.linkedin.com/in/raulirivca/");
      }
        return (
            <div>
                <Menu >
                    <Menu.Item style={{marginLeft:"90px", color:'black'}}><Link to={pathsRaul.Raul}>Raul</Link></Menu.Item>
                    <Menu.Item><Link to={pathsRaul.Services}>Servicios</Link></Menu.Item>
                </Menu>
                <div  className="body">
                    <h1>Este soy yo</h1>
                    <div className="photo">
                        <img className="borde" src={photo} alt="My face"/>
                    </div> 
                    <div className="button-section">
                        <div
                            onClick={gitlab}
                            className="button-space ui vertical animated button "
                            tabIndex="0"
                        >
                        <div className="hidden content">GitLab</div>
                            <div className="visible content">
                                <i className="gitlab icon"></i>
                            </div>
                        </div>
                        <div
                            onClick={linkedin}
                            className="ui vertical animated button button-space"
                            tabIndex="0"
                        >
                            <div className="hidden content">LinkedIn</div>
                            <div className="visible content">
                                <i className="linkedin icon"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="block">
                    <h3>Nombre: Raul Rivera Castañeda</h3>
                    <h3>Codigo:20192560D</h3>
                    <h3>Carrera:Ingeniería de Sistemas</h3>
                    <h3> Ciclo Académico: 5to Ciclo</h3>
                </div>
            </div>
        );  
      }
